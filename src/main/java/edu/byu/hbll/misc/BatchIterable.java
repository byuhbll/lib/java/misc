package edu.byu.hbll.misc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Function;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import lombok.AllArgsConstructor;
import lombok.experimental.UtilityClass;

/**
 * Wraps an {@link Iterable} and creates an {@link Iterator} that collects elements from the
 * underlying {@link Iterator} into a batch before returning next.
 *
 * @param <T> the element type
 * @author Charles Draper
 */
public class BatchIterable<T> implements Iterable<List<T>> {

  private Iterable<T> iterable;
  private double capacity = 10;
  private Function<T, Number> scale = t -> 1L;

  /**
   * Creates a new {@link BatchIterable}.
   *
   * @param iterable the underlying {@link Iterable}
   */
  public BatchIterable(Iterable<T> iterable) {
    this.iterable = iterable;
  }

  @Override
  public Iterator<List<T>> iterator() {
    return new BatchIterator<>(iterable.iterator(), capacity, scale, null);
  }

  /**
   * Sets the maximum weight or capacity of the batch. The capacity is equal to the number of
   * elements in the batch unless otherwise denoted in {@link #scale(Function)}. Default capacity is
   * 10.
   *
   * @param capacity the batch capacity
   * @return this
   */
  public BatchIterable<T> capacity(double capacity) {
    this.capacity = capacity;
    return this;
  }

  /**
   * The function for calculating the weight of each element. The returned {@link Number} value will
   * be converted to a double using {@link Number#doubleValue()} before testing against the
   * capacity. The default function always returns a weight of 1.
   *
   * <p>Note if the weight of a single element exceeds the batch capacity, the element will be in a
   * batch of its own.
   *
   * @param scale function for measuring weight
   * @return this
   */
  public BatchIterable<T> scale(Function<T, Number> scale) {
    this.scale = scale;
    return this;
  }

  /**
   * A convenience method for creating a stream from this {@link Iterable}.
   *
   * @return the stream
   */
  public Stream<List<T>> stream() {
    return StreamSupport.stream(
        Spliterators.spliteratorUnknownSize(iterator(), Spliterator.ORDERED), false);
  }

  /**
   * Convenience methods for instantiating a {@link BatchIterable}.
   *
   * @author Charles Draper
   */
  @UtilityClass
  public static class Batch {

    /**
     * Creates an {@link Iterable} of batches given the {@link Iterable}.
     *
     * @param <T> the element type
     * @param iterable the iterable to group into batches
     * @return the batching iterable
     */
    public static <T> BatchIterable<T> of(Iterable<T> iterable) {
      return new BatchIterable<>(iterable);
    }

    /**
     * Creates a one-time {@link Iterable} of batches given the {@link Iterator}.
     *
     * @param <T> the element type
     * @param iterator the iterator to group into batches
     * @return the batching iterable
     */
    public static <T> BatchIterable<T> of(Iterator<T> iterator) {
      return of(() -> iterator);
    }

    /**
     * Creates a one-time {@link Iterable} of batches given the {@link Stream}.
     *
     * @param <T> the element type
     * @param stream the stream to group into batches
     * @return the batching iterable
     */
    public static <T> BatchIterable<T> of(Stream<T> stream) {
      return of(stream.iterator());
    }
  }

  /**
   * Iterator responsible for creating batches out of an inner iterator. Note that if a single
   * element exceeds the batch capacity, the element is included as the only element in the batch.
   *
   * @author Charles Draper
   * @param <T> the type of elements returned in batches by this iterator
   */
  @AllArgsConstructor
  private static class BatchIterator<T> implements Iterator<List<T>> {

    private final Iterator<T> iterator;
    private final double capacity;
    private final Function<T, Number> scale;

    private T next;

    @Override
    public boolean hasNext() {
      return next != null || iterator.hasNext();
    }

    @Override
    public List<T> next() {
      if (!hasNext()) {
        throw new NoSuchElementException();
      }

      List<T> batch = new ArrayList<>();
      double size = 0;

      // ensure the batch hasn't reached capacity and there are more elements to add and there is
      // room left in the batch array or there was a leftover element from last iteration
      while (size < capacity && iterator.hasNext() && batch.size() < Integer.MAX_VALUE
          || next != null) {
        // use the leftover item from last iteration if there was one
        T next = this.next != null ? this.next : iterator.next();
        batch.add(next);

        // add the weight of the item to the size of this batch
        size += scale.apply(next).doubleValue();
        this.next = null;
      }

      // sometimes the last element added pushes the batch over the capacity, this removes that last
      // element unless it is the only element in the batch
      if (size > capacity && batch.size() > 1) {
        next = batch.remove(batch.size() - 1);
      }

      return Collections.unmodifiableList(batch);
    }
  }
}
