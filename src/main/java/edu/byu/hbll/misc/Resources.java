package edu.byu.hbll.misc;

import java.io.IOException;
import java.io.InputStream;
import java.net.JarURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import lombok.experimental.UtilityClass;

/**
 * Utilities for working with classpath resources.
 *
 * <p>The extract methods were derived from contributions made on stackoverflow:
 *
 * <p>https://stackoverflow.com/questions/1386809/copy-directory-from-a-jar-file
 *
 * <p>https://stackoverflow.com/questions/3923129/get-a-list-of-resources-from-classpath-directory
 *
 * <p>https://stackoverflow.com/questions/20105554/is-there-a-way-to-tell-if-a-classpath-resource-is-a-file-or-a-directory
 *
 * @author Charles Draper
 */
@UtilityClass
public class Resources {

  /**
   * Recursively extracts the given classpath resource to a temporary location. The extracted
   * resource will be deleted when the virtual machine terminates.
   *
   * @param resourceName name of the resource to be extracted
   * @return path to the extracted resource
   * @throws IOException if an I/O error occurs
   */
  public static Path extract(String resourceName) throws IOException {
    Path tmpDir = Files.createTempDirectory("resource.");
    tmpDir.toFile().deleteOnExit();
    return extract(resourceName, tmpDir, true);
  }

  /**
   * Recursively extracts the given classpath resource into the given destination directory.
   *
   * @param resourceName name of the resource to be extracted
   * @param destination a directory where the resource will be extracted
   * @param deleteOnExit if true, the extracted resource will be deleted when the virtual machine
   *     terminates.
   * @return path to the extracted resource
   * @throws IOException if an I/O error occurs
   */
  public static Path extract(String resourceName, Path destination, boolean deleteOnExit)
      throws IOException {
    if (!Files.isDirectory(destination)) {
      throw new IllegalArgumentException("destination must be a directory");
    }

    URL url = getResourceUrl(resourceName);
    URLConnection urlConnection = url.openConnection();

    if (urlConnection instanceof JarURLConnection) {
      copy((JarURLConnection) urlConnection, destination, deleteOnExit);
    } else {
      try {
        copy(Paths.get(url.toURI()), destination, deleteOnExit);
      } catch (URISyntaxException e) {
        // shouldn't happen
        throw new RuntimeException(e);
      }
    }

    return destination.resolve(resourceName.replaceFirst(".*/", ""));
  }

  /**
   * Recursively copies the given source path to the destination path.
   *
   * @param src the source path to copy
   * @param dest the destination to copy into
   * @throws IOException if an I/O error occurs
   */
  private static void copy(Path src, Path dest, boolean deleteOnExit) throws IOException {
    Path srcParent = src.getParent();

    for (Path childSrc : (Iterable<Path>) Files.walk(src)::iterator) {
      Path childDest = dest.resolve(srcParent.relativize(childSrc));

      if (Files.isDirectory(childSrc)) {
        childDest.toFile().mkdirs();
      } else {
        Files.copy(childSrc, childDest, StandardCopyOption.REPLACE_EXISTING);
      }

      if (deleteOnExit) {
        childDest.toFile().deleteOnExit();
      }
    }
  }

  /**
   * Recursively copies the given jar resource to the destination path.
   *
   * @param jarConnection connection to the jar file
   * @param dest the destination to copy into
   * @throws IOException if an I/O error occurs
   */
  private static void copy(JarURLConnection jarConnection, Path dest, boolean deleteOnExit)
      throws IOException {

    JarFile jarFile = jarConnection.getJarFile();
    String entryName = jarConnection.getEntryName();
    String parentEntryName = entryName.replaceFirst("(.+)/.+", "$1");
    parentEntryName = entryName.contains("/") ? parentEntryName : "";

    for (JarEntry entry : (Iterable<JarEntry>) jarFile.entries()::asIterator) {
      String name = entry.getName();

      if (name.startsWith(entryName)) {
        Path childDest = Paths.get(dest.toString(), name.substring(parentEntryName.length()));

        if (entry.isDirectory()) {
          childDest.toFile().mkdirs();
        } else {
          Files.copy(jarFile.getInputStream(entry), childDest, StandardCopyOption.REPLACE_EXISTING);
        }

        if (deleteOnExit) {
          childDest.toFile().deleteOnExit();
        }
      }
    }
  }

  /**
   * Returns the URL corresponding to the classpath resource.
   *
   * @param resourceName the resource name
   * @return the corresponding url
   */
  public static URL getResourceUrl(String resourceName) {
    final URL in = Thread.currentThread().getContextClassLoader().getResource(resourceName);
    return in == null ? Resources.class.getResource(resourceName) : in;
  }

  /**
   * Reads the contents of the given resource to a byte array.
   *
   * @param resourceName name of the resource to read
   * @return the byte array
   * @throws IOException if an I/O error occurs
   */
  public static byte[] readBytes(String resourceName) throws IOException {
    try (InputStream in = getResourceUrl(resourceName).openStream()) {
      return in.readAllBytes();
    }
  }

  /**
   * Reads the contents of the given resource to a String.
   *
   * @param resourceName name of the resource to read
   * @return the string
   * @throws IOException if an I/O error occurs
   */
  public static String readString(String resourceName) throws IOException {
    return new String(readBytes(resourceName));
  }

  /**
   * Reads the contents of the given resource to a String using the supplied charset.
   *
   * @param resourceName name of the resource to read
   * @param charset the charset to use
   * @return the string
   * @throws IOException if an I/O error occurs
   */
  public static String readString(String resourceName, Charset charset) throws IOException {
    return new String(readBytes(resourceName), charset);
  }
}
