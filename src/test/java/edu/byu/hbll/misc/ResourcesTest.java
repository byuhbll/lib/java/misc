package edu.byu.hbll.misc;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.junit.jupiter.api.Test;

/**
 * @author Charles Draper
 */
class ResourcesTest {

  /**
   * Test method for {@link edu.byu.hbll.misc.Resources#extract(java.lang.String)}.
   *
   * @throws IOException
   */
  @Test
  void testExtractString() throws IOException {
    Path extractedPath;

    // test target/classes extract for dir
    extractedPath = Resources.extract("/resources");
    assertEquals(
        Set.of("resources/", "resources/1/1.1", "resources/2", "resources/1"),
        listFiles(extractedPath));

    // test target/classes extract for file
    extractedPath = Resources.extract("/resources/1/1.1");
    assertEquals(Set.of("1.1"), listFiles(extractedPath));

    // test jar extract for dir
    extractedPath = Resources.extract("/org/junit/jupiter/api");
    assertTrue(
        listFiles(extractedPath)
            .containsAll(List.of("api/Assertions.class", "api/extension/Extensions.class")));

    // test jar extract for file
    extractedPath = Resources.extract("/org/junit/jupiter/api/Assertions.class");
    assertTrue(listFiles(extractedPath).containsAll(List.of("Assertions.class")));
  }

  /**
   * More fully tested by {@link #testExtractString()}.
   *
   * <p>Test method for {@link edu.byu.hbll.misc.Resources#extract(java.lang.String,
   * java.nio.file.Path, boolean)}.
   *
   * @throws IOException
   */
  @Test
  void testExtractStringPathBoolean() throws IOException {
    Path tmpDir = Files.createTempDirectory("resource.");
    tmpDir.toFile().deleteOnExit();
    Path extractDir = Resources.extract("/resources", tmpDir, true);
    assertEquals(tmpDir.resolve("resources"), extractDir);
  }

  /** Test method for {@link edu.byu.hbll.misc.Resources#getResourceUrl(java.lang.String)}. */
  @Test
  void testGetResourceUrl() {
    assertNotNull(Resources.getResourceUrl("/resources"));
    assertEquals(Resources.getResourceUrl("/resources"), Resources.getResourceUrl("resources"));
    assertNotNull(Resources.getResourceUrl("/org/junit/jupiter/api"));
    assertEquals(
        Resources.getResourceUrl("/org/junit/jupiter/api"),
        Resources.getResourceUrl("org/junit/jupiter/api"));
  }

  /**
   * Test method for {@link edu.byu.hbll.misc.Resources#readBytes(java.lang.String)}.
   *
   * @throws IOException
   */
  @Test
  void testReadBytes() throws IOException {
    assertArrayEquals(new byte[] {70, 105, 108, 101, 32, 50}, Resources.readBytes("/resources/2"));
    assertTrue(Resources.readBytes("/org/junit/jupiter/api/Assertions.class").length > 0);
  }

  /**
   * Test method for {@link edu.byu.hbll.misc.Resources#readString(java.lang.String)}.
   *
   * @throws IOException
   */
  @Test
  void testReadStringString() throws IOException {
    assertEquals("File 2", Resources.readString("/resources/2"));
  }

  /**
   * Test method for {@link edu.byu.hbll.misc.Resources#readString(java.lang.String,
   * java.nio.charset.Charset)}.
   *
   * @throws IOException
   */
  @Test
  void testReadStringStringCharset() throws IOException {
    assertEquals("File 2", Resources.readString("/resources/2", StandardCharsets.UTF_8));
  }

  /**
   * Recursively lists all files in a directory and relatives their paths to the directory. If file,
   * return file name.
   *
   * @param dir the directory
   * @return list of relatived paths
   * @throws IOException
   */
  private Set<String> listFiles(Path dir) throws IOException {
    if (Files.isDirectory(dir)) {
      Set<String> list =
          Files.walk(dir)
              .map(f -> dir.relativize(f).toString())
              .map(f -> f.replace(File.separatorChar, '/'))
              .map(f -> dir.toFile().getName() + "/" + f)
              .collect(Collectors.toSet());
      return list;
    } else {
      return Set.of(dir.toFile().getName());
    }
  }
}
