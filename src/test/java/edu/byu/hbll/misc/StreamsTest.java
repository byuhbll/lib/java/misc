package edu.byu.hbll.misc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;

/** Unit tests for {@link Streams}. */
public class StreamsTest {

  /**
   * Verifies that attempting to run {@link Streams#of(Iterable)} with a {@code null} {@link
   * Iterable} will fail.
   */
  @Test
  public void shouldFailToCreateStreamFromNullIterable() {
    assertThrows(NullPointerException.class, () -> Streams.of((Iterable<String>) null));
  }

  /**
   * Verifies that {@link Streams#of(Iterable)} will create a new {@link Stream} from the given
   * {@link Iterable}.
   */
  @Test
  public void shouldCreateStreamFromIterable() {
    Iterable<String> iterable = Arrays.asList("a", "b", "c");

    Set<String> set = Streams.of(iterable).collect(Collectors.toSet());
    assertEquals(3, set.size());
  }

  /**
   * Verifies that attempting to run {@link Streams#of(Iterator)} with a {@code null} {@link
   * Iterator} will fail.
   */
  @Test
  public void shouldFailToCreateStreamFromNullIterator() {
    assertThrows(NullPointerException.class, () -> Streams.of((Iterator<String>) null));
  }

  /**
   * Verifies that {@link Streams#of(Iterator)} will create a new {@link Stream} from the given
   * {@link Iterator}.
   */
  @Test
  public void shouldCreateStreamFromIterator() {
    Iterator<String> iterator = Arrays.asList("a", "b", "c").iterator();

    Set<String> set = Streams.of(iterator).collect(Collectors.toSet());
    assertEquals(3, set.size());
  }
}
