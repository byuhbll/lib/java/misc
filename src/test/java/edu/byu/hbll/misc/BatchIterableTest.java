package edu.byu.hbll.misc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import edu.byu.hbll.misc.BatchIterable.Batch;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;
import org.junit.jupiter.api.Test;

/**
 * @author Charles Draper
 */
class BatchIterableTest {

  private static final List<Integer> list = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13);

  /** Test method for {@link edu.byu.hbll.misc.BatchIterable#of(java.lang.Iterable)}. */
  @Test
  void testOfIterableOfT() {
    int loop = 1;

    for (List<Integer> batch : Batch.of(list)) {
      testBatching(batch, loop++);
    }
  }

  /** Test method for {@link edu.byu.hbll.misc.BatchIterable#of(java.util.Iterator)}. */
  @Test
  void testOfIteratorOfT() {
    int loop = 1;

    for (List<Integer> batch : Batch.of(list.iterator())) {
      testBatching(batch, loop++);
    }
  }

  /** Test method for {@link edu.byu.hbll.misc.BatchIterable#of(java.util.stream.Stream)}. */
  @Test
  void testOfStreamOfT() {
    int loop = 1;

    for (List<Integer> batch : Batch.of(list.stream())) {
      testBatching(batch, loop++);
    }
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.misc.BatchIterable.BatchIterable2#BatchIterable(java.lang.Iterable)}.
   */
  @Test
  void testBatchIterable() {
    int loop = 1;

    for (List<Integer> batch : new BatchIterable<>(list)) {
      testBatching(batch, loop++);
    }
  }

  /** Test method for {@link edu.byu.hbll.misc.BatchIterable.BatchIterable2#iterator()}. */
  @Test
  void testIterator() {
    int loop = 1;
    Iterator<List<Integer>> it = Batch.of(list).iterator();

    while (it.hasNext()) {
      testBatching(it.next(), loop++);
    }

    assertThrows(NoSuchElementException.class, () -> it.next());
  }

  /** Test method for {@link edu.byu.hbll.misc.BatchIterable.BatchIterable2#capacity(long)}. */
  @Test
  void testCapacity() {
    int loop = 0;

    for (List<Integer> batch : Batch.of(list).capacity(3)) {
      assertEquals(list.subList(loop * 3, Math.min(loop * 3 + 3, list.size())), batch);
      loop++;
    }
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.misc.BatchIterable.BatchIterable2#scale(java.util.function.Function)}.
   */
  @Test
  void testScale() {
    int loop = 1;

    for (List<Integer> batch : Batch.of(list).capacity(100).scale(n -> Math.pow(n, 1.8))) {
      switch (loop) {
        case 1:
          assertEquals(List.of(1, 2, 3, 4, 5, 6), batch);
          break;
        case 2:
          assertEquals(List.of(7, 8), batch);
          break;
        default:
          assertEquals(List.of(loop + 6), batch);
      }

      loop++;
    }
  }

  /** Test method for {@link edu.byu.hbll.misc.BatchIterable.BatchIterable2#stream()}. */
  @Test
  void testStream() {
    List<List<Integer>> batches = Batch.of(list).stream().collect(Collectors.toList());
    testBatching(batches.get(0), 1);
    testBatching(batches.get(1), 2);
  }

  private void testBatching(List<Integer> batch, int loop) {
    if (loop == 1) {
      assertEquals(list.subList(0, 10), batch);
    } else {
      assertEquals(list.subList(10, list.size()), batch);
    }
  }
}
