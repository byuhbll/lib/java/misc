# Java Miscellaneous

This library is a collection of small one-off java classes that are not reasonably formed into their own repository. This library has no dependencies other than Java SE. For other more complicated libraries consider creating a separate repository.

## Usage

Add this library as a dependency to your project's pom.xml.

```xml
<dependency>
  <groupId>edu.byu.hbll</groupId>
  <artifactId>misc</artifactId>
  <version>1.5.0</version>
</dependency>
```

## BatchExecutorService

A type of executor service that processes collections (batches) of items in separate threads. Processing items in a batch can be much more efficient than processing each item individually. As items are submitted to this executor, the items are placed in a batch which is then given to a separate thread to be processed. Having separate threads process the batch decouples the submission and the actual execution. The executor returns a `Future` so the item can be tracked through the process and an optional result returned.

```java
BatchRunnable<Integer, Double> r = b -> b.stream().map(e -> Math.pow(e, 2)).collect(Collectors.toList());
BatchExecutorService<Integer, Double> executor = new BatchExecutorService.Builder(r).build();

Future<Double> f1 = executor.submit(1);
Future<Double> f2 = executor.submit(2);
Future<Double> f3 = executor.submit(3);

System.out.println(f1.get());
System.out.println(f2.get());
System.out.println(f3.get());

executor.shutdownAndWait();
```

## BatchIterable

Returns elements in batches from an underlying `Iterable`.

```java
List<Integer> list = List.of(1, 2, 3, 4, 5);

for(List<Integer> batch : new BatchIterable<>(list).capacity(2)) {
   System.out.println(list);
}

```

For convenience, you may also invoke the `BatchIterable` with:

```java
for(List<Integer> batch : Batch.of(list)) {
```

## PrimaryMonitor

A monitor that can be used to determine which instance of an application in a multi-instance deployment should perform background processing that is inherently single threaded.

```java
public class Harvester {

  private PrimaryMonitor monitor;

  public void init(){
    PrimaryMonitorDatabase db = new MongoPrimaryMonitorDatabase();
    monitor = new PrimaryMonitor.Builder(db).build();
  }

  public void shutdown(){
    monitor.shutdown();
  }

  public void harvest(){
    if(!harvest.isPrimary()){
      return;
    }
    ...
  }
}
```

## Common Helper Functions

A number of common functions for **Strings**, **Lists**, **Optionals**, **Resources**, and **Streams** are available as static helper methods inside the five above-named classes.  Among included functions are null-safe and empty-safe methods for returning the first or last element of a List, null-safe methods for asserting that a given String is non-blank (having at least one non-whitespace character), or methods for creating non-parallel Streams from Iterables or Iterators.

Future versions of Java may support some of these operations natively; as this happens, the convenience methods provided here will be deprecated to encourage use of the standard tools.

## Developing This Project

Note: These instructions assume Docker and VS Code, with the Dev Containers extension, are installed.

Git clone the project locally and open it with VS Code. When prompted, reopen the project in a dev container. It will take a couple of minutes to download all of the dependencies and prepare the container for Java development. Proceed with development once the initialization is complete.

Tests can be run from within VS Code. Click the Testing icon and click "Run Tests".

To build and install the project locally in order to include it in another project for testing, run the following from the project directory on the host machine.

```
docker run -it -u $UID:$(id -g) -e MAVEN_CONFIG=/var/maven/.m2 -v ~/.m2:/var/maven/.m2 -v .:/project maven mvn -f /project/pom.xml -Duser.home=/var/maven clean package install
```

Building and deploying this project is handled by the GitLab pipeline. There are two possible destinations for the built project: the BYU HBLL internal Maven repository or the central Maven repository. Use the internal repository for non-public releases and the central repository for public open source releases. To instruct the pipeline to deploy to Maven Central, set the `MAVEN_CENTRAL_DEPLOY` CI/CD variable to `true`. Deploying to either the internal or central repository requires manually playing the deploy job. Note: Deployments to Maven Central are permanent.

## License

[License](LICENSE.md)